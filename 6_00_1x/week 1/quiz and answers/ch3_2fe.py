#Let s be a string that contains a sequence of decimal numbers separated by commas,
#e.g., s = '1.2,2.4,3.123'. Write a program that prints the sum of numbers in s.
s = input('Please enter numbers separated by commas')
splitNumbers = s.split(',')
count = 0
for i in splitNumbers:
    count = count + float(i)
print(str(count) + ' is the sum of numbers entered')

