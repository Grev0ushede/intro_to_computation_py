#What would the code listed below do if the statement x = 25 was replaced by a negative int
#x = 25
#epsilon = 0.01
#numGuesses = 0
#low = 0.0
#high = max(1.0, x)
#ans = (high + low)/2.0
#while abs(ans**2 - x) >= epsilon:
#    print('low =', low, 'high =', high, 'ans =', ans)
#    numGuesses += 1
#    if ans**2 < x:
#        low = ans
#    else:
#        high = ans
#    ans = (high + low)/2.0
#print('numGuesses =', numGuesses)
#print(ans, 'is close to the square root of', x)

#It produces an infinite loop by repeating the else argument

#What would have to be changed to make the code above work for finding an aprox
#to the cube root of both negative and positive numbers?(Hint: think about changing
#low to ensure that the answer lies within the region being searched.)

x = -25
epsilon = 0.01
numGuesses = 0
low = 0.0
high = max(1.0, x)
ans = (high + low)/2.0
while abs(ans**2 - x) >= epsilon:
    print('low =', low, 'high =', high, 'ans =', ans)
    numGuesses += 1
    if ans**2 < x:
        low = ans
    else:
        high = ans
    ans = (high + low)/2.0
print('numGuesses =', numGuesses)
print(ans, 'is close to the square root of', x)
