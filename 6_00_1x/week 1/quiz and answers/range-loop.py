s = "abcdefghijk"
for index in range(len(s)):
    if s[index] == 'i' or s[index] == 'u':
        print("This string contains letters i or u")
        break
for char in s:
    if char == 'i' or char == 'u':
        print("This also checks for i or u")
        break

