#assume s is a string of lowercase characters
#write a program that counts up the number of vowels contained in
#the string s. valid vowels are a,e,i,o,u. for example if
# s='azcbobobegghakl'
s = 'azcbobobegghakl'
vowels = 0
for i in s:
    if i == 'a' or i == 'e' or i == 'i' or i == 'o' or i == 'u':
        vowels += 1
print("Number of vowels: " + str(vowels))