#Assume to variables, 'varA' and 'varB', are assigned values, numbers or
#strings. Write a piece of code that evaluates varA and varB, and then
#prints out one of the following messages:
#"string involved" if either "varA" or "varB"
#"bigger" if "varA" is larger than "varB"
#"equal" if "varA" is equal to "varB"
#"smaller" if "varA" is smaller than "varB"

if type(varA) == str or type(varB) == str:
    print("string involved")
elif varA < varB
    print("smaller")
elif varA > varB
    print("bigger")
else:
    print("equal")