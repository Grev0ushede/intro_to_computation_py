#Convert the following into code that uses a while loops
# prints 2,4,6,8,10,Goodbye!
num = 2
while num < 11: #or <= 10
    print(num)
    num += 2
print("Goodbye!")

# Here is one from the answers that I did not think of
#num = 1
#while num <= 10:
#    if num % 2 == 0:
#        print(num)
#    num += 1
#print("Goodbye!")

#convert the following...
#prints Hello, 10, 8, 6, 4, 2

num = 10
print("Hello!")
while num > 0:
    print (num)
    num -= 2

#Here is the above answer as well
#num = 10
#print("Hello")
#while num > 0:
#   if num % 2 == 0:
#       print(num)
#   num -= 1

#Write a while loop that sums the values 1 through end, inclusive.
#'end' is a predefined variable. so if we define end to be 6, your code
#should print out 21, which is '1+2+3+4+5+6'

end = 21
x = 0

while end > 0:
    x += end
    end -= 1
print(x)