#Convert the following code into code that uses a for loop
#prints 2,4,6,8,10,'Goodbye!'

for i in range(2,11,2):
    print(i)
print("Goodbye!")

#prints "Hello!",10,8,6,4,2

print("Hello!")
for i in range(10,0,-2):
    print(i)

#write a for loop that sums values 1 through end, inclusive
#so if we define end to be 6, your code should print
#"21" which is 1+2+3+4+5+6

num = 0
for i in range(1, end+1):
    num += i
print(num)